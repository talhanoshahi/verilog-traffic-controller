module traffic_controller
#(
	parameter RESET_HIGH = 1,
	parameter RESET_LOW = 0,

	parameter ACTIVE_GREEN_BUSY = 30,
	parameter ACTIVE_GREEN_NORMAL = 20,
	parameter ACTIVE_YELLOW = 5
)
(
// tc means traffic controller
	input tc_clock,
	input tc_reset,

	output reg [11:0] tc_led_status
	// all red = 12'b100100100100
	// all green = 12'b001001001001
	// all yellow = 12'b010010010010
	// lane 1 green = 12'b100100100001
	// lane 1 yellow = 12'b100100100010
	// lane 2 green = 12'b100100001100
	// lane 2 yellow = 12'b100100010100
	// lane 3 green = 12'b100001100100
	// lane 3 yellow = 12'b100010100100
	// lane 4 green = 12'b001100100100
	// lane 4 yellow = 12'b010100100100
);

reg [2:0] tc_current_led;
reg [4:0] tc_clock_interval;
reg [1:0] tc_next_lan_green;

reg reset_high;

localparam LED_PER_LAN = 3;

localparam LANE_1 = 0;
localparam LANE_2 = 1;
localparam LANE_3 = 2;
localparam LANE_4 = 3;
localparam TOTAL_LANS = 4;

localparam LED_HIGH = 0;
localparam LED_LOW = 1;

localparam RED = 3'b100;
localparam GREEN = 3'b001;
localparam YELLOW = 3'b010;

localparam LED_PLUS_LANS = 12;

always @(posedge tc_clock)
begin
	if (tc_reset == RESET_HIGH)
	begin
		tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] = RED;
		tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)] = RED;
		tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)] = RED;
		tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)] = RED;

		tc_clock_interval = 0;
		tc_current_led = RED;
		reset_high = 1;
		tc_next_lan_green = 0;
	end

	else if (tc_reset == RESET_LOW)
	begin
		if (reset_high == 1)
		begin
			tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] = GREEN;
			tc_current_led = GREEN;
			tc_clock_interval = 0;
			reset_high = 0;
		end

		tc_clock_interval = tc_clock_interval + 1;

		if (tc_clock_interval == ACTIVE_GREEN_BUSY && tc_current_led == GREEN)
		begin
			if (tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] == tc_current_led)
			begin
				tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] = YELLOW;
				tc_current_led = YELLOW;

				tc_clock_interval = 0;
			end

			else if (tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)] == tc_current_led)
			begin
				tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)] = YELLOW;
				tc_current_led = YELLOW;

				tc_clock_interval = 0;
			end
		end

		if (tc_clock_interval == ACTIVE_GREEN_NORMAL && tc_current_led == GREEN)
		begin
			if (tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)] == tc_current_led)
			begin
				tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)] = YELLOW;
				tc_current_led = tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)];

				tc_clock_interval = 0;
			end

			if (tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)] == tc_current_led)
			begin
				tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)] = YELLOW;
				tc_current_led = tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)];

				tc_clock_interval = 0;
			end
		end

		if (tc_clock_interval == ACTIVE_YELLOW && tc_current_led == YELLOW)
		begin
			if (tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] == tc_current_led)
			begin
				tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] = RED;
				tc_current_led = tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1];
				tc_next_lan_green = LANE_2;

				tc_clock_interval = 0;
			end
			else if (tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)] == tc_current_led)
			begin
				tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)] = RED;
				tc_current_led = tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)];
				tc_next_lan_green = LANE_3;

				tc_clock_interval = 0;
			end
			else if (tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)] == tc_current_led)
			begin
				tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)] = RED;
				tc_current_led = tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)];
				tc_next_lan_green = LANE_4;

				tc_clock_interval = 0;
			end
			else if (tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)] == tc_current_led)
			begin
				tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)] = RED;
				tc_current_led = tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)];
				tc_next_lan_green = LANE_1;

				tc_clock_interval = 0;
			end

		end

		else if (tc_clock_interval == ACTIVE_YELLOW && tc_current_led == RED)
		begin
			case (tc_next_lan_green)
				LANE_1: begin
					tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1] = GREEN;
					tc_current_led = tc_led_status[(LANE_2 * LED_PER_LAN) - 1:LANE_1];

					tc_clock_interval = 0;
				end
				LANE_2: begin
					tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)] = GREEN;
					tc_current_led = tc_led_status[(LANE_3 * LED_PER_LAN) - 1:(LANE_2 * LED_PER_LAN)];

					tc_clock_interval = 0;
			end
			LANE_3: begin
				tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)] = GREEN;
				tc_current_led = tc_led_status[(LANE_4 * LED_PER_LAN) - 1:(LANE_3 * LED_PER_LAN)];

				tc_clock_interval = 0;
			end
			LANE_4: begin
				tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)] = GREEN;
				tc_current_led = tc_led_status[LED_PLUS_LANS - 1:(LANE_4 * LED_PER_LAN)];

				tc_clock_interval = 0;
			end
			endcase
		end

	end
end
endmodule
