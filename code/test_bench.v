module test_bench ();
// tb stands for test bench
reg tb_clock;
reg tb_reset;

wire [11:0] tb_led_status;

localparam HIGH_RESET = 1;
localparam LOW_RESET = 0;

traffic_controller tb_cc (tb_clock, tb_reset, tb_led_status);

initial begin 
	$dumpfile ("project.vcd");
	$dumpvars (1, test_bench);

	tb_clock = 0; tb_reset = HIGH_RESET;

	#10 tb_reset = LOW_RESET;
	#1500 $finish;
	
end

always begin
	#5 tb_clock = ~tb_clock;
end

endmodule
